import requests
import movie_data
import datetime
import calendar


class Movie:
    def __init__(self, title, date, rating, description, photo, id):
        self.title = title
        self.rating = rating
        self.description = description
        self.photo = photo
        self.id = id
        dt_format = "%Y-%m-%d"
        self.datetime = datetime.datetime.strptime(date, dt_format).date()
        self.month = self.datetime.strftime("%m")
        self.date = self.datetime.strftime(f'%d/{calendar.month_name[int(self.month)]}/%Y')


movies = []
for i in range(0, 5):
    movies.append(Movie(title=movie_data.data[i]['title'],
                        date=movie_data.data[i]['release_date'],
                        rating=movie_data.data[i]['vote_average'],
                        description=movie_data.data[i]['overview'],
                        photo=f'https://image.tmdb.org/t/p/{"w500"}{movie_data.data[i]["backdrop_path"]}',
                        id=movie_data.data[i]['id']))
