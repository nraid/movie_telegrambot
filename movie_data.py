import requests
import json
from random import randrange

MOVIE_TOKEN = 'xxxx' # MOVIEDB token
URL = f'https://api.themoviedb.org/3/movie/upcoming?api_key={MOVIE_TOKEN}&language=en-US&page=1'


response = requests.get(URL)
response.raise_for_status()

data = response.json()['results']
json_string = json.dumps(data)
