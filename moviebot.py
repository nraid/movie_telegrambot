from typing import Final
import telegram
from telegram import Update
from telegram.ext import Application, CommandHandler, MessageHandler, filters, ContextTypes
import movie_data
import requests
import datetime
import calendar
from movie_logic import movies

TOKEN: Final = 'xxxxx' # Telegram token
BOT_USERNAME: Final = 'xxxxx'


# Commands

async def start_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text(
        'Hello. Right now all I can do is to give you short info about 10 ongoing or upcoming films.'
        ' More functions in future.'
        ' If you need help just type /help or /show.')


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text('The only viable command is /show. Try it out.')


async def show_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    for i in range(0, 5):
        try:
            await context.bot.send_photo(update.message.chat_id,
                                         photo=movies[i].photo,
                                         parse_mode='Markdown',
                                         caption=f'''
            \n🎞️ *{movies[i].title}*
            \n🎭Average rating:  *{movies[i].rating}*
            \n📅Release date:  *{movies[i].date}*
            \n\n🍿Description:  {movies[i].description}''')
        except telegram.error.TelegramError as e300:
            try:
                # Trying to change size for 'w300'
                movies[i].photo = f'https://image.tmdb.org/t/p/w300{movie_data.data[i]["poster_path"]}'
                await context.bot.send_photo(update.message.chat_id,
                                             photo=movies[i].photo,
                                             parse_mode='Markdown',
                                             caption=f'''
                            \n🎞️ *{movies[i].title}*
                            \n🎭Average rating:  *{movies[i].rating}*
                            \n📅Release date:  *{movies[i].date}*
                            \n\n🍿Description:  {movies[i].description}''')

            except telegram.error.TelegramError as e300:
                print(f" :D {movies[i].title}, {movies[i].photo}, {e300}")
                try:
                    # Trying to change size for 'w400'
                    movies[i].photo = f'https://image.tmdb.org/t/p/w400{movie_data.data[i]["poster_path"]}'
                    await context.bot.send_photo(update.message.chat_id,
                                                 photo=movies[i].photo,
                                                 parse_mode='Markdown',
                                                 caption=f'''
                                \n🎞️ *{movies[i].title}*
                                \n🎭Average rating:  *{movies[i].rating}*
                                \n📅Release date:  *{movies[i].date}*
                                \n\n🍿Description:  {movies[i].description}''')
                except telegram.error.TelegramError as e400:
                    print(f" :D {movies[i].title}, {movies[i].photo}, {e400}")
                    pass


def handle_response(text: str) -> str:
    processed: str = text.lower()

    if 'hello' in processed:
        return 'sup'


async def handle_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
    message_type: str = update.message.chat.type
    text: str = update.message.text

    print(f'User ({update.message.chat.id}) in {message_type}: "{text}"')

    if message_type == 'group':
        if BOT_USERNAME in text:
            new_text: str = text.replace(BOT_USERNAME, '').strip()
            response: str = handle_response(new_text)
        else:
            return
    else:
        response: str = handle_response(text)

    print('Bot:', response)
    await update.message.reply_text(response)


async def error(update: Update, context: ContextTypes.DEFAULT_TYPE):
    print(f'Update {update} caused error {context.error}')


if __name__ == '__main__':
    print('Starting bot...')
    app = Application.builder().token(TOKEN).build()

    # Commands
    app.add_handler(CommandHandler('start', start_command))
    app.add_handler(CommandHandler('help', help_command))
    app.add_handler(CommandHandler('show', show_command))

    # Messages
    app.add_handler(MessageHandler(filters.TEXT, handle_message))

    # Errors
    app.add_error_handler(error)

    # Polls the bot
    print('Polling...')
    app.run_polling(poll_interval=3)
